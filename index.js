const express = require("express");
// require mongoose package
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// Connection to MongoDB Atlas
// Syntax: mongoose.connect("<MongoDB Atlas connection string>", { useNewUrlParser:true });
mongoose.connect("mongodb+srv://dbkatlegaspi:KYrn2aegKYEdVN2J@wdc028-course-booking.5gl23.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

// If a connection error occurs, output will be shown in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output will be shown in the console
db.once("open", () => console.log("We're connected to the cloud database."));

// Schema - determines the structure of the documents to be written in the database
// Instantiate schema
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
});

// Create the Task model

const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name : req.body.name}, (err, result) =>{
		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found.");
		}
		else{
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created.");
				}
			})
		}
	})
});

// Create a GET request to retrieve all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		// If an error occured
		if(err){
			return console.log(err);
		}
		// If no errors are found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

// ================ START of ACTIVITY ================
// Create a User schema.
const userSchema = new mongoose.Schema({
	username : String,
	password : String
});

// Create a User model.
const User = mongoose.model("User", userSchema);

// Create a POST route that will access the "/signup" route that will create a user.
app.post("/signup", (req, res) => {
	User.findOne({username : req.body.username}, (err, result) =>{
		
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate username found. Try again");
		}
		else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New username registered.");
				}
			})
		}
	})
});
// ================ END of ACTIVITY ================

app.listen(port, () => console.log(`Server is running at port ${port}.`));